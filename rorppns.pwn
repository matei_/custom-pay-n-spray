/*

	Pay'N'Spray Custom creat de matei_ si mmnf. pentru Romania Roleplay

*/


#include <a_samp>
#include <zcmd>
#include <streamer>


#if defined CreateDynamicObject
	#define PNS_CreateObject CreateDynamicObject
#else
	#define PNS_CreateObject CreateObject
#endif


#define COLOR_WHITE 	(0xFFFFFFFF)
#define COL_WHITE 		"{FFFFFF}"

#define COLOR_SERVER 	(0xBB9955FF)
#define COL_SERVER 		"{BB9955}"

#define COLOR_TOMATO 	(0xFF6347FF)
#define COL_TOMATO 		"{FF6347}"

#define COLOR_YELLOW 	(0xFFDD00FF)
#define COL_YELLOW 		"{FFDD00}"

#define COLOR_GREEN 	(0x00FF00FF)
#define COL_GREEN 		"{00FF00}"

#define COLOR_EMERALD 	(0x2ECC71FF)
#define COL_EMERALD 	"{2ECC71}"

#define COLOR_DEFAULT 	(0xA9C4E4FF)
#define COL_DEFAULT 	"{A9C4E4}"

#define script:%0(%1) \
			forward %0(%1); public %0(%1)

static const Float: EnterRepairPoint[9][3] = {
	{	1965.5773, 2162.1806, 10.4348	},
	{	719.8439, -464.8626, 16.3382	},
	{	-100.0154, 1109.6612, 19.7366	},
	{	-1421.2109, 2593.5109, 55.7228	},
	{	-1904.3210, 275.3920, 41.0413	},
	{	488.4949, -1732.7858, 10.8613	},
	{	1024.9555, -1031.4205, 31.9482	},
	{	2073.2937, -1831.5445, 13.5413	},
	{	-2425.3593, 1030.5458, 50.3851	}
};
static const Float: PNSGates[16][6] = {
	{	2071.5512, -1830.9199, 14.9076, 0.8000, -0.2999, 89.6000	},
	{	2644.8762, -2039.1561, 12.9270, 0.0000, 0.0000, 0.0000		},
	{	488.1740, -1735.3991, 12.4605, -0.0999, 0.6000, 172.9999	},
	{	1025.2313, -1029.3850, 33.9652, 0.0000, 0.0000, 0.0000		},
	{	1042.2940, -1026.0196, 33.0591, 0.0000, 0.0000, 0.0000		},
	{	1843.3498, -1855.5008, 12.2811, 0.0000, 0.0000, -88.8000	},
	{	720.1838, -462.5781, 17.1067, 0.0000, 0.0000, 0.0000		},
	{	-2716.1865, 217.5251, 5.6626, -0.4999, 0.3999, 90.0000		},
	{	-1936.6575, 239.2837, 35.5392, 0.0000, 0.0000, 0.0000		},
	{	-1904.5925, 277.7711, 42.5020, 0.0000, 0.0000, 0.0000		},
	{	-2425.9335, 1028.1936, 52.4635, 0.0000, 0.0000, 179.5997	},
	{	-1786.9392, 1209.4191, 26.3924, 0.0000, 0.0000, 0.0000		},
	{	-1420.5354, 2590.9758, 57.1904, 0.0000, 0.0000, -178.5999	},
	{	-100.1009, 1111.7559, 20.9467, 0.0000, 0.0000, 0.0000		},
	{	1968.3216, 2162.7075, 11.9833, 0.0000, 0.0000, -90.0000		},
	{	2386.4187, 1043.4499, 12.2033, 0.0000, 0.0000, -0.2999 		}
};

static const Float: RepairPointPickup[9][3] = {
	{	 1965.5773, 2162.1806, 10.4348	},
	{	-1904.3210, 275.3920, 41.0413	},
	{	-1421.2109, 2593.5109, 55.7228	},
	{	1024.9555, -1031.4205, 31.9482	},
	{	-2425.3593, 1030.5458, 50.3851	},
	{	-100.0154, 1109.6612, 19.7366	},
	{	719.8439, -464.8626, 16.3382	},
	{	488.4949, -1732.7858, 10.8613	},
	{	2073.2937, -1831.5445, 13.5413	}
};

#define FILTERSCRIPT

public OnFilterScriptInit() {
	LoadPickups();
	LoadGates();
	return 1;
}

public OnFilterScriptExit() {
	return 1;
}

CMD:enterpns(playerid, params[]) {

	if(GetSVarInt("inUse") == 1)
		return SendClientMessage(playerid, COLOR_TOMATO, "Acest Pay'N'Spray este deja folosit. Foloseste o alta locatie pentru a-ti repara vehiculul.");

	if(GetPlayerMoney(playerid) < 1000)
		return SendClientMessage(playerid, COLOR_TOMATO, "Nu ai 1000$ pentru a-ti repara vehiculul.");

	if(!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COLOR_TOMATO, "Pentru a avea acces intr-un Pay'N'Spray trebuie sa te aflii intr-un vehicul.");

	if(!RepairPoint(playerid))
		return SendClientMessage(playerid, COLOR_TOMATO, "Nu te aflii in zona unui Pay'N'Spray.");

	new Float: vHP, vID = GetPlayerVehicleID(playerid);
	GetVehicleHealth(vID, vHP);

	if(vHP >= 1000)
		return SendClientMessage(playerid, COLOR_TOMATO, "Vehiculul tau nu are nevoie de reparatii.");
	if(vHP < 400)
		return SendClientMessage(playerid, COLOR_TOMATO, "Vehiculul tau a suferit prea mult damage, te rugam sa contactezi un mecanic.");
	
	EnterRepair(playerid);
	return 1;
}


stock EnterRepair(playerid) {
	new vID;
	vID = GetPlayerVehicleID(playerid);	

	// The Quarter Red Sands East - Las Venturas
	if(IsPlayerInRangeOfPoint(playerid, 7.0, 1965.5773, 2162.1806, 10.4348)) {
		SetVehiclePos(vID, 1976.0337, 2162.3547, 11.0703);
		SetTimerEx("RepairTimerLV", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	// Dillimore
	if(IsPlayerInRangeOfPoint(playerid, 7.0, 719.8439, -464.8626, 16.3382)) {
		SetVehiclePos(vID, 719.6019, -457.2534, 16.3359);
		SetTimerEx("RepairTimerDillimore", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	// Fort Carson
	if(IsPlayerInRangeOfPoint(playerid, 7.0, -100.0154, 1109.6612, 19.7366)) {
		SetVehiclePos(vID, -100.3473, 1118.4963, 19.7417);
		SetTimerEx("RepairTimerFortCarson", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	// El Quebrados
	if(IsPlayerInRangeOfPoint(playerid, 7.0, -1421.2109, 2593.5109, 55.7228)) {
		SetVehiclePos(vID, -1420.3120, 2583.8464, 55.8433);
		SetTimerEx("RepairTimerElQuerbrados", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	// San Fierro
	if(IsPlayerInRangeOfPoint(playerid, 7.0, -1904.3210, 275.3920, 41.0413)) {
		SetVehiclePos(vID, -1904.4828, 283.5549, 41.0469);
		SetTimerEx("RepairTimerSF", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	// Santa Maria Beach - Los Santos
	if(IsPlayerInRangeOfPoint(playerid, 7.0, 488.4949, -1732.7858, 10.8613)) {
		SetVehiclePos(vID, 487.5885, -1741.0295, 11.1373);
		SetTimerEx("RepairTimerSantaMaria", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	// Temple - Los Santos
	if(IsPlayerInRangeOfPoint(playerid, 7.0, 1024.9555, -1031.4205, 31.9482)) {
		SetVehiclePos(vID, 1025.0090, -1023.1766, 32.1016);
		SetTimerEx("RepairTimerTemple", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
	}
	// Idlewood - Los Santos
	if(IsPlayerInRangeOfPoint(playerid, 7.0, 2073.2937, -1831.5445, 13.5413)) {
		SetVehiclePos(vID, 2065.5515, -1831.3530, 13.5469);
		SetTimerEx("RepairTimerIdlewood", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	// Juniper Hollow - San Fierro
	if(IsPlayerInRangeOfPoint(playerid, 7.0, -2425.3593, 1030.5458, 50.3851)) {
		SetVehiclePos(vID, -2425.4280, 1021.1931, 50.3977);
		SetTimerEx("RepairTimerJuniperSF", 10000, false, "i", playerid);
		GivePlayerMoney(playerid, -1000);
		PlayerPlaySound(playerid, 1134, 0.0, 0.0, 0.0);
		TogglePlayerControllable(playerid, 0);
		SendClientMessage(playerid, COLOR_WHITE, "Ai fost taxat cu 1000$ pentru reparatiile vehiculului tau.");
		SetSVarInt("inUse", 1);
	}
	return 1;
}

stock RepairPoint(playerid) {
	for(new i = 0; i < sizeof(EnterRepairPoint); i ++) {
		if(IsPlayerInRangeOfPoint(playerid, 7.0, EnterRepairPoint[i][0], EnterRepairPoint[i][1], EnterRepairPoint[i][2]))
			return 1;
	}
	return 0;
}

script: RepairTimerLV(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, 1962.2800, 2161.8003, 10.8203);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerDillimore(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, 720.5970, -467.5006, 16.3437);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerFortCarson(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, -99.7338, 1107.0349, 19.7422);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerElQuerbrados(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, -1421.4509, 2596.4739, 55.6875);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerSF(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, -1906.2386, 273.2421, 41.0391);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerSantaMaria(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, 489.0434, -1730.1194, 11.1304);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerTemple(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, 1025.6643, -1033.8490, 31.7557);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerIdlewood(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, 2076.4961, -1831.6530, 13.5545);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

script: RepairTimerJuniperSF(playerid) {
	new vID;
	RepairVehicle(GetPlayerVehicleID(playerid));
	vID = GetPlayerVehicleID(playerid);
	SetVehiclePos(vID, -2424.1780, 1032.3905, 50.3906);
	TogglePlayerControllable(playerid, 1);
	DeleteSVar("inUse");
	return 1;
}

stock LoadPickups() {
	for(new i = 0; i < sizeof(RepairPointPickup); i ++) {
		CreatePickup(1239, 1, RepairPointPickup[i][0], RepairPointPickup[i][1], RepairPointPickup[i][2], -1);
		Create3DTextLabel("Iti poti repara vehiculul aici\nTasteaza comanda "COL_SERVER"/enterpns"COL_WHITE, COLOR_WHITE, RepairPointPickup[i][0], RepairPointPickup[i][1], RepairPointPickup[i][2], 30.0, 0, 1);
	}
	return 1;
}

stock LoadGates() {
	for(new i = 0; i < sizeof(PNSGates); i ++ ) {
		CreateObject(971, PNSGates[i][0], PNSGates[i][1], PNSGates[i][2], PNSGates[i][3], PNSGates[i][4], PNSGates[i][5]);
	}
	return 1;
}